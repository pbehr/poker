var dealer = {
  
  init: function() {
    this.shuffleCards();
    this.determineDealer();
  },

  start: function() {
    this.init();
  },

  shuffleCards: function() {
    var m = 52, t, i;
    this.deck = [
      'H1','H2','H3','H4','H5','H6','H7','H8','H9','H10','H11','H12','H13',
      'D1','D2','D3','D4','D5','D6','D7','D8','D9','D10','D11','D12','D13',
      'C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','C11','C12','C13',
      'S1','S2','S3','S4','S5','S6','S7','S8','S9','S10','S11','S12','S13'
    ];

    do {
      i = Math.floor(Math.random() * m--);
      t = this.deck[m];
      this.deck[m] = this.deck[i];
      this.deck[i] = t;
    } while(m);
    // convert each card into a Card object
    this.deck.forEach( function(elem, index){ this.deck[index] = new Card(elem); });
  },

  determineDealer: function() {
    this.dealCards( 1, true, function(){
      var cardValue = 0;
      var highest = 0;
      game.players.forEach( function(player, index) {
        cardValue = player.cards[0].getValue();
        // An Ace counts as high card
        if (cardValue === 1) { cardValue = 14; }
        if (cardValue > highest) {
          highest = cardValue;
          game.dealer = index;
        }
      });
      game.setGameMessage('Player ' + (game.dealer+1) + ' is the dealer.', 4);
    });
  },



  dealCards: function(...args) {
    var numberOfCards = 2,
        faceUp = false,
        callback = undefined,
        players = [];

    args.each( function(value, index){
      switch (typeof value) {
        case 'number' :
          numberOfCards = value;
          break;
        case 'boolean':
          faceUp = value;
          break;
        case 'function' :
          callback = value;
          break;
      }
    });
    if (numberOfCards > 2) { numberOfCards = 2; }
    if (numberOfCards < 1) { numberOfCards = 1; }
    var dealCard = function() {
      var card = this.deck.pop();
      var player = this.getNextPlayer();
      player.takeCard( card );
      if (player.isLivePlayer || this.faceUp ) {
        card.show();
      } else {
        card.hide();
      }
      playerCount++;
      // If not every player has gotten a card yet, call deal card again
      if (playerCount < hand.players.length) {
        setTimeout( function(){ dealCard(); }, 300);
      } else {
        // If all the players have gotten a card, and we're supposed
        // to give them more than 1 card, then start dealing all over
        if (cardCount < numberOfCards) {
          cardCount++;
          playerCount = 0;
          setTimeout( function(){ dealCard(); }, 300);
        } else {
          // all cards have been dealt...call the callback function
          if (typeof callback === 'function') {
            callback();
          }
        }
      }
    }
    // Kick off the process...
    dealCard();
  },


  rakeCards: function() {
    hand.players.forEach( function(player) { player.rakeCards(); } );
    hand.communityCards.forEach( function(card) { card.remove(); card = null; });
    hand.communityCards = [];
  },


}
