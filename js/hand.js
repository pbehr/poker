//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// hand
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Hand(deck, players) {
  this.deck = deck;
  this.players = players;
};
Hand.prototype.start = function() {
  this.div = div;
};

var hand = {

  init: function() {
    hand.players = [];
    hand.blind = 0;
    hand.pot = 0;
    hand.deck = [];
    hand.communityCards = [];
    hand.step = '';
    hand.currentPlayer = -1;
  },

  start: function() {
    hand.players = game.players.slice();
    hand.blind = settings.blindSchedule[game.currentBlindNumber];
    hand.pot = 0;
    $('#pot').html('');
    hand.rakeCards();
    hand.shuffleCards();
    hand.step = 'blinds';
    if (game.dealer === undefined) {
      game.dealer = 0;
      hand.step = 'dealer';
    }
    hand.go();
  },

  go: function() {
    switch (hand.step) {
      case 'dealer':
        hand.determineDealer();
        break;
      case 'blinds':
        hand.dealer = game.dealer;
        hand.moveDealerPuck();
        hand.rakeCards();
        hand.getBlinds();
        break;
      case 'bet':
        if (hand.bigBlind != undefined) {
          hand.currentPlayer = hand.bigBlind;
        } else {
          hand.currentPlayer = hand.dealer;
        }
        hand.allAround = false;
        hand.betTimer = setTimeout( function() {hand.bet();}, 500);
        break;
      case 'deal':
        // clear all the player bets from the last round of betting
        for (var i = 0; i < hand.players.length; i++) {
          hand.players[i].clearBet();
        };
        // deal the flop, turn, or river
        if (hand.communityCards.length == 0) {
          hand.currentBet = 0;
          hand.dealFlop();
        } else if (hand.communityCards.length == 3){
          hand.currentBet = 0;
          hand.dealTurn();
        } else if (hand.communityCards.length == 4) {
          hand.currentBet = 0;
          hand.dealRiver();
        } else {
          game.setGameMessage('Hand is over, need to determine the winner');
        }
        break;
    }
  },



  getBlinds: function() {
    hand.currentPlayer = hand.dealer;
    hand.getNextPlayer().makeBet( hand.blind/2 );
    hand.getNextPlayer().makeBet( hand.blind );
    hand.bigBlind = hand.currentPlayer;
    hand.currentBet = hand.blind;
    hand.currentPlayer = hand.dealer;
    setTimeout( function() {
      hand.dealCards(2, function(){
        hand.step = 'bet';
        hand.go();
      });
    }, 1000);
  },

  bet: function() {
    hand.step = 'betting';
    var player = hand.getNextPlayer();
    player.highlightName();
    if (player.isLivePlayer) {
      hand.getPlayerBet();
    } else {
      hand.getComputerBet();
    }
  },


  increasePot: function( amount ) {
    hand.pot += amount;
    $('#pot').html('Pot: $' + hand.pot);
  },


  getNextPlayer: function() {
    hand.currentPlayer++;
    if (hand.currentPlayer == hand.players.length) {
      hand.currentPlayer = 0;
    }
    return hand.players[hand.currentPlayer];
  },

  getPrevious: function(playerNum) {
    var prev = playerNum - 1;
    if (prev < 0 ) {
      prev = hand.players.length - 1;
    }
    return prev;
  },

  getCurrentPlayer: function() {
    return hand.players[hand.currentPlayer];
  },

  getPlayerBet: function() {
    var player = hand.getCurrentPlayer();
    var minBet = hand.currentBet - player.bet;
    // The small blind needs different numbers......
    $('#betSlider').attr({
      min: minBet,
      max: player.stack,
      step: hand.blind,
      value: minBet,
    });
    hand.updateBetButton();
    // add click listeners to the buttons
    $('#foldButton')
    .unbind('click')
    .click( function(ev) {
      console.log(player);
      ev.preventDefault();
      hand.foldPlayer();
      $('#userInput').hide();
      hand.potIsRight();
    });
    $('#betButton')
    .unbind('click')
    .click( function(ev) {
      ev.preventDefault();
      player.makeBet( parseInt($('#betSlider').val()));
      player.unhighlightName();
      $('#userInput').hide();
      hand.potIsRight();
    });
    $('#userInput').show();
  },

  getComputerBet: function() {
    // For now, computer players will just call the min bet
    var player = hand.getCurrentPlayer();
    setTimeout( function() {
      player.unhighlightName();
      var betAmount = hand.currentBet - player.bet;
      if (betAmount > 0) {
         player.makeBet( betAmount );
      }
      hand.potIsRight();
    }, 2000 );
  },

  foldPlayer: function() {
    var player = hand.getCurrentPlayer();
    // remove the player from the current hand
    hand.players.splice( hand.currentPlayer, 1);
    player.rakeCards();
    player.unhighlightName();

    // we might have to change the "dealer"
    // just don't move the dealer puck...
    if (hand.dealer >= hand.currentPlayer) {
      hand.dealer = hand.getPrevious(hand.dealer);
    }
    // set the current player to the previous player
    hand.currentPlayer = hand.getPrevious(hand.currentPlayer);
  },

  updateBetButton: function() {

    //  The Small Blind needs different numbers......
    var text = '';
    var bet = parseInt( $('#betSlider')[0].value );
    var min = parseInt( $('#betSlider').attr('min') );
    var max = parseInt(hand.players[hand.currentPlayer].stack);
    switch (bet) {
      case min:
        if (min == 0) {
          text = 'Check';
        } else if (hand.currentBet){
          text = 'Call ' + bet;
        } else {
          text = 'Bet ' + bet;
        }
        break;
      case max:
        text = 'All In';
        break;
      default:
        text = 'Bet ' + bet;
    }
    $('#betButton').attr('value', text);
  },

  dealFlop: function() {
    // Get three cards for the flop
    hand.communityCards = [];
    for (var i = 0; i < 3; i++) {
      var card = hand.deck.pop();
      hand.communityCards.push(card);
      card.setDiv( '#communityCards .card' + (i + 1) );
      card.show();
    }
    $('#communityCards').show();

    // another round of betting
    setTimeout( function() {
      hand.step = 'bet';
      hand.go();
    } , 1500);
  },

  dealTurn: function() {
    var card = hand.deck.pop();
    hand.communityCards.push(card);
    card.setDiv('#communityCards .card4');
    card.show();

    // another round of betting
    setTimeout( function() {
      hand.step = 'bet';
      hand.go();
    } , 1500);
  },

  dealRiver: function() {
    var card = hand.deck.pop();
    hand.communityCards.push(card);
    card.setDiv('#communityCards .card5');
    card.show();
    // another round of betting
    setTimeout( function() {
      hand.step = 'bet';
      hand.go();
    } , 1500);

  },

  potIsRight: function() {
    var isRight = true;
    if (!hand.allAround) {
      if ( ((hand.bigBlind != undefined) && hand.currentPlayer == hand.bigBlind)
      ||   ((hand.bigBlind == undefined) && hand.currentPlayer == hand.dealer)){
        hand.allAround = true;
        hand.bigBlind = undefined;
      } else {
        isRight = false;
      }
    }

    if (hand.allAround) {
      for (var idx in hand.players) {
        if (hand.players[idx].bet != hand.currentBet) {
          isRight = false;
          break;
        }
      }
    }

    // If the pot's right, deal the next card(s)
    if (isRight) {
      hand.step = 'deal';
      setTimeout( function() {
        hand.go();
      }, 1500 );

    // If the pot's not right, keep betting.
    } else {
      setTimeout( function() {
        hand.bet();
      }, 500 );

    }





  },

  moveDealerPuck: function() {
    var top, left;
    switch (hand.dealer) {
      case 0:
        top = 450;
        left = 550;
        break;
      case 1:
        top = 440;
        left = 350;
        break;
      case 2:
        top = 380;
        left = 200;
        break;
      case 3:
        top = 280;
        left = 130;
        break;
      case 4:
        top = 160;
        left = 230;
        break;
      case 5:
        top = 120;
        left = 630;
        break;
      case 6:
        top = 180;
        left = 790;
        break;
      case 7:
        top = 310;
        left = 860;
        break;
      case 8:
        top = 410;
        left = 750;
        break;
    }
    $('#dealerPuck').offset({'top': top, 'left': left});
  },
};
