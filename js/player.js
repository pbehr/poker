function PlayerView(div) {

};

function Player(number) {
  this.name = 'Player ' + (number + 1);
  this.id = 'player' + (number + 1);
  this.stack = settings.startingStack;
  this.bet = 0;
  this.cards = [];
  this.isLivePlayer = false;

  var playerDiv = $('<div/>', {
    id: this.id,
    class: 'player',
  }).appendTo('#players');

  $('<div/>', {
    class: 'playerName',
    html: this.name
  }).appendTo(playerDiv);

  $('<div/>', {
    id: this.id + 'Stack',
    class: 'playerStack',
    html: this.stack
  }).appendTo(playerDiv);

  $('<div/>', {
    class: 'card card1',
  }).appendTo(playerDiv);

  $('<div/>', {
    class: 'card card2',
  }).appendTo(playerDiv);

  $('<div/>', {
    class: 'bet',
  }).appendTo(playerDiv);

};

Player.prototype.rakeCards = function() {
  this.cards.forEach( function(card) { card.remove(); card = null; });
  this.cards = [];
};


Player.prototype.hideCards = function() {
  this.cards.forEach( function(card) { card.hide(); });
};

Player.prototype.showCards = function() {
  this.cards.forEach( function(card) { card.show(); });
};

Player.prototype.highlightName = function() {
  $('#' + this.id + ' .playerName').addClass('highlight');
};

Player.prototype.unhighlightName = function() {
  $('#' + this.id + ' .playerName').removeClass('highlight');
};

Player.prototype.takeCard = function(card) {
  if (this.cards.length === 0) {
    card.setDiv( '#' + this.id + ' .card1' );
    this.cards.push( card );
  } else if (this.cards.length === 1) {
    card.setDiv('#' + this.id + ' .card2');
    this.cards.push( card );
  }

};

Player.prototype.updateInfo = function() {
  $('#' + this.id + 'Stack').html(this.stack);
  if (this.bet > 0) {
    $('#' + this.id + ' .bet').html('$' + this.bet);
  } else {
    $('#' + this.id + ' .bet').html('');
  }

};

Player.prototype.getCards = function() {
  return this.cards;
}

Player.prototype.getName = function() {
  return this.name;
}

Player.prototype.makeBet = function( amount ) {
  if (this.stack < amount) { amount = this.stack; }
  this.bet += amount;
  this.stack -= amount;
  this.updateInfo();
  if(this.bet > hand.currentBet) {
    hand.currentBet = this.bet;
  }
  hand.increasePot(amount);
}

Player.prototype.clearBet = function() {
  this.bet = 0;
  this.updateInfo();
}
