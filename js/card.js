function Card(suitValue) {
  this.suit = suitValue.substring(0,1);
  this.value = parseInt( suitValue.substring(1));
};

Card.prototype.setDiv = function( div ) {
  this.div = div;
};

Card.prototype.getBackgroundPosition = function()  {
  var valuePos = 44 * (this.value -1) * -1;
  var suitPos = 62 * (Card.convertSuitToNumber(this.suit) - 1) * -1;
  return valuePos + 'px ' + suitPos + 'px';
};

Card.prototype.get = function() {
return this.suit + this.value;
};

Card.prototype.getSuit = function() {
return this.suit;
};

Card.prototype.getValue = function() {
return this.value;
};

Card.prototype.show = function() {
  var bg = this.getBackgroundPosition();
  $(this.div).css('background-position', bg);
  $(this.div).show();
};

Card.prototype.hide = function() {
  $(this.div).css('background-position', '0px -248px');
  $(this.div).show();
};

Card.prototype.remove = function() {
  $(this.div).hide();
};


Card.convertNumberToSuit = function(number) {
  var suit = '';
  switch (number) {
    case 1:
      suit = 'H';
      break;
    case 2:
      suit = 'D';
      break;
    case 3:
      suit = 'C';
      break;
    case 4:
      suit = 'S';
      break;
  }
  return suit;
};

Card.convertSuitToNumber = function(suit) {
  var number = 0;
  switch (suit) {

    case 'H':
      number = 1;
      break;
    case 'D':
      number = 2;
      break;
    case 'C':
      number = 3;
      break;
    case 'S':
      number = 4;
      break;
  }
  return parseInt( number );
};
