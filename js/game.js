$(window).load(function() {
  game.showGameStartScreen();
});


var game = {
  showGameStartScreen: function() {
    $('.gamelayer').hide();
    $('#gamestartscreen').show();
    game.init();
    settings.init();
  },

  showHandStartScreen: function() {
    console.log('end of hand screen');
  },

  showGameEndScreen: function() {
    console.log('END OF GAME');
  },

  start: function() {
    $('.gamelayer').hide();
    $('#gamescreen').show();
    if (game.players.length === 0) {
      game.createPlayers();
    }
    if (!game.blindTimer) {
      game.blindTimer = setTimeout( function() {game.incrementBlinds();}, settings.blindInterval * 60000);
    }
    dealer.init();
    dealer.start();
  },

  createPlayers: function() {
    for (var x=0; x < settings.numberOfPlayers; x++) {
      game.players.push( new Player(x) );
    };
    game.players[0].isLivePlayer = true;
  },

  setGameMessage: function(msg, removeSeconds) {
    removeSeconds = removeSeconds || 3.5;
    if (!game.currentMessage) {
      game.currentMessage = msg;
      $('#messages').text(msg);
      $('#messages').show();
      setTimeout( function() {game.clearGameMessage();}, removeSeconds * 1000);
    } else {
      setTimeout( function() {game.setGameMessage(msg, removeSeconds);}, .5);
    }
  },

  clearGameMessage: function() {
    game.currentMessage = undefined;
    $('#messages').hide();
  },

  incrementBlinds: function() {
    if (++game.currentBlindNumber < settings.blindSchedule.length) {
      var bigBlind = settings.blindSchedule[game.currentBlindNumber];
      var smallBlind = bigBlind / 2;
      game.setGameMessage('Blinds increased to ' + smallBlind + '/' + bigBlind);
      game.blindTimer = setTimeout( function() {game.incrementBlinds();}, settings.blindInterval * 60000);
    }
  },

  init: function(){
    game.currentBlindNumber = 0;
    game.dealer = 0;
    game.players = [];
    clearTimeout(game.blindTimer);
    game.blindTimer = undefined;
  },
}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// settings
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
var settings = {
  numberOfPlayers : 2,
  startingStack : 1000,
  blindInterval: 2,
  blindSchedule : [10, 20, 30, 40, 50, 80, 100, 120, 150, 200, 300, 400, 600],
  init: function() {

    var playerCountValue = $('#playerCountValue');
	  $('#playerCount')[0].oninput = (function(){
	    playerCountValue.html(this.value);
      settings.numberOfPlayers = this.value;
	  });
	  $('#playerCount')[0].oninput();

    var stackValue = $('#stackValue');
	  $('#stack')[0].oninput = (function(){
	    stackValue.html(this.value);
      settings.startingStack = this.value;
	  });
	  $('#stack')[0].oninput();

    var blindIntervalValue = $('#blindIntervalValue');
	  $('#blindInterval')[0].oninput = (function(){
	    blindIntervalValue.html(this.value);
      settings.blindInterval = this.value;
	  });
	  $('#blindInterval')[0].oninput();
  },
}
